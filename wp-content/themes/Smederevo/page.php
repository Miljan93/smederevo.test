<?php get_header(); ?>
<?php get_template_part('partials/mobile-header'); ?> 
<main>
    <div class="container">
    <h1><?php the_title() ?></h1>
        <div class="single-wrapper">
            <p><?php the_content() ?></p>
        </div>
    </div>
</main>
<?php get_footer(); ?>