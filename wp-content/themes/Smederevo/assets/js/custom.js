(function($) {

    var questionsFieldsIsLoading = false;
    var questionsFieldsIsLoaded = false;
    $(document).ready(function() {
        $("#questions-fields-tab").trigger('click');
    });
    $(document).on('click', '#back-to-questions-fields', function(){
        $("#questions-fields-tab").trigger('click');
    });
    
	$(document).on('click', '#questions-fields-tab', function(){
        if(!questionsFieldsIsLoading && !questionsFieldsIsLoaded){
            questionsFieldsIsLoading = true;
            var that = $(this);
            var ajaxurl = that.data('url');
            var post_types = that.data('post-types');
            $("#spinner").css("display", "flex");
            $('#questions-content').empty();
            $.ajax({
                url : ajaxurl,
                type : 'post',
                data : {
                    action : 'load_questions_fields',
                    post_types : post_types
                },
                error : function( response ){
                    console.log(response);
                },
                success : function( response ){
                    if(response){
                        $('#answers').removeClass('show active');
                        $('#questions-fields').addClass('show active');
                        $('#questions-fields-content').append( response );
                    }
                },
                complete: function(){
                    questionsFieldsIsLoading = false;
                    questionsFieldsIsLoaded = true;
                    $("#spinner").css("display", "none"); 
                }
            });
        }
        else{
            $('#answers').removeClass('show active');
            $('#questions-fields').addClass('show active');
        }
    });


    var answersIsLoading = false;
    $(document).on('click', '.answers', function(){
        if(!answersIsLoading){
            answersIsLoading = true;
            var that = $(this);
            var ajaxurl = that.data('url');
            var post_type = that.data('post-type');
            var page_id = that.data('page-id');
            $("#spinner").css("display", "flex");
            $('#questions-fields').removeClass('show active');
            if(checkLocalAnswers(page_id)){
                scroll();
                $("#spinner").css("display", "none");
                $('#answers').addClass('show active');
                $('#answers-content').empty();
                $('#answers-content').append( checkLocalAnswers(page_id) );
                answersIsLoading = false;
            }
            else{
                $.ajax({
                    url : ajaxurl,
                    type : 'post',
                    data : {
                        action : 'load_answers',
                        post_type : post_type,
                        page_id : page_id
                    },
                    error : function( response ){
                        console.log(response);
                    },
                    success : function( response ){
                        if(response){
                            $('#answers').addClass('show active');
                            $('#answers-content').empty();
                            $('#answers-content').append( response );
                            cacheLocalAnswers(page_id, response);
                            scroll();
                        }
                    },
                    complete: function(){
                        answersIsLoading = false;
                        $("#spinner").css("display", "none"); 
                    }
                });
            }  
        }
    });

    var localAnswersCache = [];

    function cacheLocalAnswers(page_id, response){
        if(localAnswersCache.find(element => element.page_id == page_id)){
            
        }
        else{
            localAnswersCache.push({page_id: page_id, respons: response});
        }
    }

    function checkLocalAnswers(page_id){
        let element = localAnswersCache.find(element => element.page_id == page_id);
        if(element){
            return element.respons;
        }
        else{
            return false;
        }
    }

    function scroll(){
        if($(window).width() <= 560) {
            $('html, body').animate({
                scrollTop: $(".nav-tabs").offset().top
            }, 500);
        }
    }

})( jQuery );