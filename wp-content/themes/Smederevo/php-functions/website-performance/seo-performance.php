<?php

// Uklanjanje opcije za SEO optimizaciju za korisnike koji nisu administratori
function wpse_init(){
    if( !check_user_role('administrator') ){
        // Remove page analysis columns from post lists, also SEO status on post editor
        add_filter('wpseo_use_page_analysis', '__return_false');
        // Remove Yoast meta boxes
        add_action('add_meta_boxes', 'disable_seo_metabox', 100000);
    }   
}
add_action('init', 'wpse_init');

// Proveravanje korisničkih rola
function check_user_role( $role, $user_id = null ) {
    if ( is_numeric( $user_id ) )
        $user = get_userdata( $user_id );
    else
        $user = wp_get_current_user();
    if ( empty( $user ) )
        return false;
    return in_array( $role, (array) $user->roles );
}
// Uklanjanje opcije iz editora
function disable_seo_metabox(){
	$args=array(
		'public'                => true,
		'exclude_from_search'   => false,
		'_builtin'              => false
		); 
	$output = 'names'; // names or objects, note names is the default
	$operator = 'and'; // 'and' or 'or'
	$post_types = get_post_types($args,$output,$operator);
	foreach ($post_types as $post_type ):
		remove_meta_box('wpseo_meta', $post_type, 'normal');
		remove_meta_box('__block_editor_compatible_meta_box', $post_type, 'normal');	
	endforeach;
}

?>