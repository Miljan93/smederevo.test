<?php
/* Nuliranje stilova */
function remove_all_styles() {
	if ( ! is_admin() ) {
		global $wp_styles;
		$wp_styles->queue = array();
	}
}
add_action('wp_print_styles', 'remove_all_styles', 90);
/* Nuliranje stilova - KRAJ*/

/* Nuliranje skripti */
function remove_all_scripts() {
	if ( ! is_admin() ) {
		global $wp_scripts;
		$wp_scripts->queue = array();
	}
}
add_action('wp_print_scripts', 'remove_all_scripts', 90);
/* Nuliranje skripti - KRAJ*/

/* Registrovanje skripti u Futeru */
function add_footer_styles() {
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/assets/css/bootstrap.css');
	wp_enqueue_style( 'custom-css', get_template_directory_uri() . '/assets/css/smederevo-style.css');
	wp_enqueue_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css');
	wp_enqueue_script( 'jquery','','', 10);
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js');
	wp_enqueue_script( 'custom', get_template_directory_uri() . '/assets/js/custom.js');
}
add_action( 'get_footer', 'add_footer_styles' , 100);
/* Registrovanje skripti u Futeru - KRAJ */
?>