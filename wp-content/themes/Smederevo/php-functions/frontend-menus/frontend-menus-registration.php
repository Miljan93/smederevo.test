<?php

function custom_menus_registration() {
    register_nav_menus(
        array(
        'stanovnici' => __( 'Становници' ),
        'posetioci' => __( 'Посетиоци' ),
        'privrednici' => __( 'Привредници' ),
        'lokalna-samouprava' => __( 'Локална самоуправа' ),
        'servisne-informacije' => __( 'Сервисне информације' ),
        )
    );
}
add_action( 'init', 'custom_menus_registration' );

?>