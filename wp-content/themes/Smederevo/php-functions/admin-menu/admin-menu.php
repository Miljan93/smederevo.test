<?php

/* Uklanjanje podrazumevanog tipa članka iz menija */
add_action( 'admin_menu', 'remove_default_post_type' );

function remove_default_post_type() {
    remove_menu_page( 'edit.php' );
    remove_menu_page( 'edit.php?post_type=page' );
	if(!current_user_can('administrator'))
		remove_menu_page( 'themes.php' );
        remove_menu_page('edit.php?post_type=page');
}

add_filter( 'register_post_type_args', function( $args, $name )
{
    if( 'post' === $name || 'page' === $name )
    {   
        // $args['show_ui']        = false; // Display the user-interface
        $args['show_in_nav_menus'] = false; // Display for selection in navigation menus
        $args['show_in_menu']      = false; // Display in the admin menu
        $args['show_in_admin_bar'] = false; // Display in the WordPress admin bar
    }
    return $args;
}, 10, 2 );

/* Uklanjanje opcije "dodaj novi" za podrazumevani tip članka iz trake sa akcijama */
add_action( 'admin_bar_menu', 'remove_default_post_type_menu_bar', 999 );

function remove_default_post_type_menu_bar( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'new-post' );
	$wp_admin_bar->remove_node( 'new-page' );
	$wp_admin_bar->remove_node( 'new-revision' );
	$wp_admin_bar->remove_node( 'new-nav_menu_item' );
	$wp_admin_bar->remove_node( 'new-oembed_cache' );
	$wp_admin_bar->remove_node( 'new-oembed_cache' );
	$wp_admin_bar->remove_node( 'new-user_request' );
}

/* Uklanjanje vidžeta za brzo dodavanje podrazumevanog tipa članka */
add_action( 'wp_dashboard_setup', 'remove_draft_widget', 999 );

function remove_draft_widget(){
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
}

/* Dodavanje menija za registrovane tipove članaka */

function opste_strane_admin_menu() {
    add_menu_page(
       	'Опште стране',
        'Опште стране',
        'edit_theme_options',
        'edit.php?post_type=page',
        '',
        'dashicons-admin-page',
        2 // Pozicija
    );
}
add_action( 'admin_menu', 'opste_strane_admin_menu' );

function strane_admin_menu() {
    add_menu_page(
        'Стране',
        'Стране',
        'read',
        'strane',
        '',
        'dashicons-admin-page',
        3 // Pozicija
    );
}
add_action( 'admin_menu', 'strane_admin_menu' );

function servisi_admin_menu() {
    add_menu_page(
        'Сервиси',
        'Сервиси',
        'read',
        'servisi',
        '',
        'dashicons-admin-tools',
        4 // Pozicija
    );
}
add_action( 'admin_menu', 'servisi_admin_menu' );

function vesti_admin_menu() {
    add_menu_page(
        'Вести',
        'Вести',
        'read',
        'vesti',
        '',
        'dashicons-format-aside',
        5 // Pozicija
    );
}
add_action( 'admin_menu', 'vesti_admin_menu' );

function obavestenja_admin_menu() {
    add_menu_page(
        'Обавештења',
        'Обавештења',
        'read',
        'obavestenja',
        '',
        'dashicons-megaphone',
        6 // Pozicija
    );
}
add_action( 'admin_menu', 'obavestenja_admin_menu' );

function najave_dogadjaja_admin_menu() {
    add_menu_page(
        'Најаве догађаја',
        'Најаве догађаја',
        'read',
        'najave-dogadjaja',
        '',
        'dashicons-megaphone',
        7 // Pozicija
    );
}
add_action( 'admin_menu', 'najave_dogadjaja_admin_menu' );

function gradske_aktuelnosti_admin_menu() {
    add_menu_page(
        'Градске актуелности',
        'Градске актуелности',
        'read',
        'gradske-aktuelnosti',
        '',
        'dashicons-schedule',
        8 // Pozicija
    );
}
add_action( 'admin_menu', 'gradske_aktuelnosti_admin_menu' );

function pitanja_gradjana_admin_menu() {
    add_menu_page(
       	'Питања грађана',
        'Питања грађана',
        'read',
        'pitanja',
        '',
        'dashicons-editor-help',
        9 // Pozicija
    );
}
add_action( 'admin_menu', 'pitanja_gradjana_admin_menu' );

function uredi_meni_admin_menu() {
    add_menu_page(
       	'Уреди мени',
        'Уреди мени',
        'edit_theme_options',
        'nav-menus.php',
        '',
        'dashicons-menu-alt3',
        10 // Pozicija
    );
}
add_action( 'admin_menu', 'uredi_meni_admin_menu' );

?>