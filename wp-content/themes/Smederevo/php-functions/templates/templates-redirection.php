<?php

add_filter( 'template_include', 'template_locator' );
function template_locator( $template ){
	if(!is_search()){
		$args = array(
			'public'   => true,
			'_builtin' => false,
		);

		$output = 'names';
		$operator = 'and'; 

		$post_types = get_post_types( $args, $output, $operator ); 

		foreach ( $post_types  as $post_type ) {
			if( is_post_type_archive( $post_type ) ){
				if( $_template = locate_template( 'archives/archive-'.$post_type.'.php' ) ){
					$template = $_template;
				}
			}
			elseif( is_page( $post_type ) ){
				if( $_template = locate_template( 'pages/page-'.$post_type.'.php' ) ){
					$template = $_template;
				}
			}
			elseif( is_singular( $post_type ) ){
				if( $_template = locate_template( 'single/single-'.$post_type.'.php' ) ){
					$template = $_template;
				}
			}
		}
		return $template;
	}
	else{
		return $template;
	}
}

?>