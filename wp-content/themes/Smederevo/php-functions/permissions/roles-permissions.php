<?php 



add_filter( 'register_post_type_args', function( $args, $name )
{
	if( current_user_can('administrator') && $args['_builtin'] == false){
		$args['show_ui'] = true;
		$args['show_in_admin_bar'] = true;
		if( $args['capability_type'] == 'page' ){
			$args['show_in_nav_menus'] = true;
		}	
	}
	else if ( current_user_can('stanovnici') && $args['_builtin'] == false ){
		if ( $name === 'stanovnici' 
			|| $name === 's-vesti' 
			|| $name === 's-obavestenja'
			|| $name === 's-servisi'
			|| $name === 's-pitanja' ){
			$args['show_ui'] = true;
			$args['show_in_admin_bar'] = true;
			if( $args['capability_type'] == 'page' ){
				$args['show_in_nav_menus'] = true;
			}
			add_filter( 'wp_get_nav_menus', function( $menus, $args ){
				foreach($menus as $key => $menu){
					if($menu->slug != 'stanovnici')
						unset($menus[$key]);
				}
				return $menus;
			}, 10, 2 );
		}			
	}
	else if ( current_user_can('posetioci') && $args['_builtin'] == false ){
		if ( $name === 'posetioci' 
			|| $name === 'po-vesti' 
			|| $name === 'po-obavestenja'
			|| $name === 'po-servisi' 
			|| $name === 'po-pitanja'){
			$args['show_ui'] = true;
			$args['show_in_admin_bar'] = true;
			if( $args['capability_type'] == 'page' ){
				$args['show_in_nav_menus'] = true;
			}
			add_filter( 'wp_get_nav_menus', function( $menus, $args ){
				foreach($menus as $key => $menu){
					if($menu->slug != 'posetioci'){
						unset($menus[$key]);
					}
						
				}
				return $menus;
			}, 10, 2 );
		}			
	}
	else if ( current_user_can('privrednici') && $args['_builtin'] == false ){
		if ( $name === 'privrednici' 
			|| $name === 'pr-vesti' 
			|| $name === 'pr-obavestenja'
			|| $name === 'pr-servisi' 
			|| $name === 'pr-pitanja' ){
			$args['show_ui'] = true;
			$args['show_in_admin_bar'] = true;
			if( $args['capability_type'] == 'page' ){
				$args['show_in_nav_menus'] = true;
			}
			add_filter( 'wp_get_nav_menus', function( $menus, $args ){
				foreach($menus as $key => $menu){
					if($menu->slug != 'privrednici')
						unset($menus[$key]);
				}
				return $menus;
			}, 10, 2 );
		}					
	}
	else if ( current_user_can('lokalna-samouprava') && $args['_builtin'] == false ){
		if ( $name === 'lokalna-samouprava' 
			|| $name === 'ls-vesti' 
			|| $name === 'ls-obavestenja'
			|| $name === 'gu-servisi' 
			|| $name === 'ls-pitanja'){
			$args['show_ui'] = true;
			$args['show_in_admin_bar'] = true;
			if( $args['capability_type'] == 'page' ){
				$args['show_in_nav_menus'] = true;
			}
			add_filter( 'wp_get_nav_menus', function( $menus, $args ){
				foreach($menus as $key => $menu){
					if($menu->slug != 'lokalna-samouprava')
						unset($menus[$key]);
				}
				return $menus;
			}, 10, 2 );
		}			
	}
	else if ( current_user_can('servisne-informacije') && $args['_builtin'] == false ){
		if ( $name === 'servisne-informacije' 
			|| $name === 'si-vesti' 
			|| $name === 'si-obavestenja'
			|| $name === 'si-servisi' 
			|| $name === 'g-pitanja'){
			$args['show_ui'] = true;
			$args['show_in_admin_bar'] = true;
			if( $args['capability_type'] == 'page' ){
				$args['show_in_nav_menus'] = true;
			}
			add_filter( 'wp_get_nav_menus', function( $menus, $args ){
				foreach($menus as $key => $menu){
					if($menu->slug != 'servisne-informacije')
						unset($menus[$key]);
				}
				return $menus;
			}, 10, 2 );
		}
		
	}
	return $args;  
}, 10, 2 );
?>