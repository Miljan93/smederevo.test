<?php

add_action( 'init', 'privrednici_strane' );
function privrednici_strane() {
	$args = [
		'label'  => esc_html__( 'Привредници - Стране', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Привредници', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Привредници', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову страну', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову страну', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Привредници', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Привредници', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'page',
		'hierarchical'        => true,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,		
		'show_in_menu'        => 'strane',
		'menu_icon'           => 'dashicons-businessperson',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'privrednici', $args );
}
add_action( 'init', 'privrednici_servisi' );
function privrednici_servisi() {
	$args = [
		'label'  => esc_html__( 'Привредници - Сервиси', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Привредници - Сервиси', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Привредници - Сервиси', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нови', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нови', 'smederevo' ),
			'new_item'           => esc_html__( 'Нови сервис', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени сервис', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај сервис', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај сервис', 'smederevo' ),
			'all_items'          => esc_html__( 'Привредници', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи сервисе', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Надређена страна', 'smederevo' ),
			'not_found'          => esc_html__( 'Сервис није пронађен.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Сервис није пронађен на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Привредници - Сервиси', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања сервиса', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'page',
		'hierarchical'        => true,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		
		'show_in_menu'        => 'servisi',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'pr-servisi', $args );
}
add_action( 'init', 'privrednici_vesti' );
function privrednici_vesti() {
	$args = [
		'label'  => esc_html__( 'Привредници - Вести', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Привредници - Вести', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Привредници - Вести', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Привредници', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Привредници - Вести', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,		
		'show_in_menu'        => 'vesti',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'pr-vesti', $args );
}

add_action( 'init', 'privrednici_obavestenja' );
function privrednici_obavestenja() {
	$args = [
		'label'  => esc_html__( 'Привредници - Обавештења', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Привредници - Обавештења', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Привредници - Обавештења', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Привредници', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Привредници - Обавештења', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,		
		'show_in_menu'        => 'obavestenja',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'pr-obavestenja', $args );
}

add_action( 'init', 'privrednici_najave_dogadjaja' );
function privrednici_najave_dogadjaja() {
	$args = [
		'label'  => esc_html__( 'Привредници - Најаве догађаја', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Привредници - Најаве догађаја', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Привредници - Најаве догађаја', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Привредници', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Привредници - Најаве догађаја', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,		
		'show_in_menu'        => 'najave-dogadjaja',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'pr-najave-dogadjaja', $args );
}

add_action( 'init', 'privrednici_gradske_aktuelnosti' );
function privrednici_gradske_aktuelnosti() {
	$args = [
		'label'  => esc_html__( 'Привредници - Градске актуелности', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Привредници - Градске актуелности', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Привредници - Градске актуелности', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Привредници', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Привредници - Градске актуелности', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,		
		'show_in_menu'        => 'gradske-aktuelnosti',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'pr-g-aktuelnosti', $args );
}

add_action( 'init', 'privrednici_pitanja' );
function privrednici_pitanja() {
	$args = [
		'label'  => esc_html__( 'Привредници - Питања', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Привредници - Питања', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Привредници - Питања', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај ново', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај ново', 'smederevo' ),
			'new_item'           => esc_html__( 'Ново питање', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени питање', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај питање', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај питање', 'smederevo' ),
			'all_items'          => esc_html__( 'Привредници', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи питања', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Надређена страна', 'smederevo' ),
			'not_found'          => esc_html__( 'Питање није пронађено.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Питање није пронађено на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Привредници - Питања', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања питања', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'page',
		'hierarchical'        => true,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		
		'show_in_menu'        => 'pitanja',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail',
			'page-attributes'
		],
	];
	register_post_type( 'pr-pitanja', $args );
}
?>