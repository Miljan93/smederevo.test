<?php

add_action( 'init', 'lokalna_samouprava_strane' );
function lokalna_samouprava_strane() {
	$args = [
		'label'  => esc_html__( 'Локална самоуправа - Стране', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Локална самоуправа', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Локална самоуправа', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову страну', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову страну', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Локална самоуправа', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Локална самоуправа', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'page',
		'hierarchical'        => true,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,	
		'show_in_menu'        => 'strane',
		'menu_icon'           => 'dashicons-businessperson',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail',
		],
	];
	register_post_type( 'lokalna-samouprava', $args );
}
add_action( 'init', 'lokalna_samouprava_servisi' );
function lokalna_samouprava_servisi() {
	$args = [
		'label'  => esc_html__( 'Локална самоуправа - Сервиси', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Локална самоуправа - Сервиси', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Локална самоуправа - Сервиси', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нови', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нови', 'smederevo' ),
			'new_item'           => esc_html__( 'Нови сервис', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени сервис', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај сервис', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај сервис', 'smederevo' ),
			'all_items'          => esc_html__( 'Локална самоуправа', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи сервисе', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Надређена страна', 'smederevo' ),
			'not_found'          => esc_html__( 'Сервис није пронађен.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Сервис није пронађен на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Локална самоуправа - Сервиси', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања сервиса', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'page',
		'hierarchical'        => true,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		
		'show_in_menu'        => 'servisi',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'ls-servisi', $args );
}
add_action( 'init', 'lokalna_samouprava_vesti' );
function lokalna_samouprava_vesti() {
	$args = [
		'label'  => esc_html__( 'Локална самоуправа - Вести', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Локална самоуправа - Вести', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Локална самоуправа - Вести', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Локална самоуправа', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Локална самоуправа - Вести', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,		
		'show_in_menu'        => 'vesti',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'ls-vesti', $args );
}

add_action( 'init', 'lokalna_samouprava_obavestenja' );
function lokalna_samouprava_obavestenja() {
	$args = [
		'label'  => esc_html__( 'Локална самоуправа - Најаве догађаја', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Локална самоуправа - Најаве догађаја', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Локална самоуправа - Најаве догађаја', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Локална самоуправа', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Локална самоуправа - Најаве догађаја', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,		
		'show_in_menu'        => 'obavestenja',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'ls-obavestenja', $args );
}

add_action( 'init', 'lokalna_samouprava_najave_dogadjaja' );
function lokalna_samouprava_najave_dogadjaja() {
	$args = [
		'label'  => esc_html__( 'Локална самоуправа - Најаве догађаја', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Локална самоуправа - Најаве догађаја', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Локална самоуправа - Најаве догађаја', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Локална самоуправа', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Локална самоуправа - Најаве догађаја', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,		
		'show_in_menu'        => 'najave-dogadjaja',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'ls-najave-dogadjaja', $args );
}

add_action( 'init', 'lokalna_samouprava_gradske_aktuelnosti' );
function lokalna_samouprava_gradske_aktuelnosti() {
	$args = [
		'label'  => esc_html__( 'Локална самоуправа - Градске актуелности', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Локална самоуправа - Градске актуелности', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Локална самоуправа - Градске актуелности', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Локална самоуправа', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Локална самоуправа - Градске актуелности', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,		
		'show_in_menu'        => 'gradske-aktuelnosti',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'ls-g-aktuelnosti', $args );
}

add_action( 'init', 'lokalna_samouprava_pitanja' );
function lokalna_samouprava_pitanja() {
	$args = [
		'label'  => esc_html__( 'Локална самоуправа - Питања', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Локална самоуправа - Питања', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Локална самоуправа - Питања', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај ново', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај ново', 'smederevo' ),
			'new_item'           => esc_html__( 'Ново питање', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени питање', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај питање', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај питање', 'smederevo' ),
			'all_items'          => esc_html__( 'Локална самоуправа', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи питања', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Надређена страна', 'smederevo' ),
			'not_found'          => esc_html__( 'Питање није пронађено.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Питање није пронађено на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Локална самоуправа - Питања', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања питања', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'page',
		'hierarchical'        => true,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		
		'show_in_menu'        => 'pitanja',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail',
			'page-attributes'
		],
	];
	register_post_type( 'ls-pitanja', $args );
}
?>
