<?php

function positronx_set_post_views($post_id) {
	if (get_post_type($post_id) == 'stanovnici' 
	|| get_post_type($post_id) == 'posetioci' 
	|| get_post_type($post_id) == 'privrednici' 
	|| get_post_type($post_id) == 'lokalna-samouprava' 
	|| get_post_type($post_id) == 'servisne-informacije'
	|| get_post_type($post_id) == 's-servisi'
	|| get_post_type($post_id) == 'po-servisi'
	|| get_post_type($post_id) == 'pr-servisi'
	|| get_post_type($post_id) == 'gu-servisi'
	|| get_post_type($post_id) == 'si-servisi') {
		$count_key = '_views_count';
    	$count = get_post_meta($post_id, $count_key, true);
    }
    if($count == '') {
        $count = 0;
        delete_post_meta($post_id, $count_key);
        add_post_meta($post_id, $count_key, '0');
    } else {
        $count++;
        update_post_meta($post_id, $count_key, $count);
    }
}
function positronx_track_post_views ($post_id) {
    if ( !is_single() ) 
    return;
     
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
     
    positronx_set_post_views($post_id);
}
add_action( 'wp_head', 'positronx_track_post_views');
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

?>