<?php
add_action( 'init', 'posetioci_strane' );
function posetioci_strane() {
	$args = [
		'label'  => esc_html__( 'Посетиоци - Стране', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Посетиоци', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Посетиоци', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову страну', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову страну', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Посетиоци', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Посетиоци', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'page',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'show_in_menu'        => 'strane',
		'menu_icon'           => 'dashicons-businessperson',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'posetioci', $args );
}
add_action( 'init', 'posetioci_servisi' );
function posetioci_servisi() {
	$args = [
		'label'  => esc_html__( 'Посетиоци - Сервиси', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Посетиоци - Сервиси', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Посетиоци - Сервиси', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нови', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нови', 'smederevo' ),
			'new_item'           => esc_html__( 'Нови сервис', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени сервис', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај сервис', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај сервис', 'smederevo' ),
			'all_items'          => esc_html__( 'Посетиоци', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи сервисе', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Надређена страна', 'smederevo' ),
			'not_found'          => esc_html__( 'Сервис није пронађен.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Сервис није пронађен на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Посетиоци - Сервиси', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања сервиса', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'page',
		'hierarchical'        => true,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		
		'show_in_menu'        => 'servisi',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
		'rewrite' => true
	];
	register_post_type( 'po-servisi', $args );
}
add_action( 'init', 'posetioci_vesti' );
function posetioci_vesti() {
	$args = [
		'label'  => esc_html__( 'Посетиоци - Вести', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Посетиоци - Вести', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Посетиоци - Вести', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Посетиоци', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Посетиоци - Вести', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,	
		'show_in_menu'        => 'vesti',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'po-vesti', $args );
}

add_action( 'init', 'posetioci_obavestenja' );
function posetioci_obavestenja() {
	$args = [
		'label'  => esc_html__( 'Посетиоци - Овавештења', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Посетиоци - Обавештења', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Посетиоци - Обавештења', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Посетиоци', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Посетиоци - Обавештења', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,		
		'show_in_menu'        => 'obavestenja',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'po-obavestenja', $args );
}

add_action( 'init', 'posetioci_najave_dogadjaja' );
function posetioci_najave_dogadjaja() {
	$args = [
		'label'  => esc_html__( 'Посетиоци - Најаве догађаја', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Посетиоци - Најаве догађаја', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Посетиоци - Најаве догађаја', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Посетиоци', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Посетиоци - Најаве догађаја', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,		
		'show_in_menu'        => 'najave-dogadjaja',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'po-najave-dogadjaja', $args );
}

add_action( 'init', 'posetioci_gradske_aktuelnosti' );
function posetioci_gradske_aktuelnosti() {
	$args = [
		'label'  => esc_html__( 'Посетиоци - Градске актуелности', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Посетиоци - Градске актуелности', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Посетиоци - Градске актуелности', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај нову', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај нову', 'smederevo' ),
			'new_item'           => esc_html__( 'Нова страна', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени страну', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај страну', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај страну', 'smederevo' ),
			'all_items'          => esc_html__( 'Посетиоци', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи стране', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Пронађене стране', 'smederevo' ),
			'not_found'          => esc_html__( 'Страна није пронађена.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Страна није пронађена на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Посетиоци - Градске актуелности', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања стране', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'post',
		'hierarchical'        => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,		
		'show_in_menu'        => 'gradske-aktuelnosti',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail'
		],
	];
	register_post_type( 'po-g-aktuelnosti', $args );
}

add_action( 'init', 'posetioci_pitanja' );
function posetioci_pitanja() {
	$args = [
		'label'  => esc_html__( 'Посетиоци - Питања', 'smederevo' ),
		'labels' => [
			'menu_name'          => esc_html__( 'Посетиоци - Питања', 'smederevo' ),
			'name_admin_bar'     => esc_html__( 'Посетиоци - Питања', 'smederevo' ),
			'add_new'            => esc_html__( 'Додај ново', 'smederevo' ),
			'add_new_item'       => esc_html__( 'Додај ново', 'smederevo' ),
			'new_item'           => esc_html__( 'Ново питање', 'smederevo' ),
			'edit_item'          => esc_html__( 'Измени питање', 'smederevo' ),
			'view_item'          => esc_html__( 'Прегледај питање', 'smederevo' ),
			'update_item'        => esc_html__( 'Ажурирај питање', 'smederevo' ),
			'all_items'          => esc_html__( 'Посетиоци', 'smederevo' ),
			'search_items'       => esc_html__( 'Претражи питања', 'smederevo' ),
			'parent_item_colon'  => esc_html__( 'Надређена страна', 'smederevo' ),
			'not_found'          => esc_html__( 'Питање није пронађено.', 'smederevo' ),
			'not_found_in_trash' => esc_html__( 'Питање није пронађено на отпаду.', 'smederevo' ),
			'singular_name'      => esc_html__( 'Посетиоци - Питања', 'smederevo' ),
            'attributes'         => esc_html__( 'Подешавања питања', 'smederevo' ),
		],
		'public'              => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'show_ui'             => false,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'show_in_rest'        => true,
		'capability_type'     => 'page',
		'hierarchical'        => true,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		
		'show_in_menu'        => 'pitanja',
		'menu_icon'           => 'dashicons-admin-multisite',
		'supports' => [
			'title',
			'editor',
			'revisions',
			'excerpt',
			'thumbnail',
			'page-attributes'
		],
	];
	register_post_type( 'po-pitanja', $args );
}
?>