<?php
/* Duplicate posts and pages function. Duplicates appear as drafts, and the user is redirected to the Edit screen. */

function rd_duplicate_post_as_draft(){
    global $wpdb;
    if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
        wp_die('No post to duplicate has been supplied!');
    }

/* Nonce verification */
if ( !isset( $_GET['duplicate_nonce'] ) || !wp_verify_nonce( $_GET['duplicate_nonce'], basename( __FILE__ ) ) )
    return;

/* This gets the original post or page ID */
$post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );

/* …then grabs the original post data. */
$post = get_post( $post_id );
     
/* To select another user as the post author, use $new_post_author = $post->post_author;. Otherwise… */
$current_user = wp_get_current_user();
$new_post_author = $current_user->ID;
/* If the post data exists, create the duplicate */
if (isset( $post ) && $post != null) {
    /* Create a new post data array */
    $args = array(
        'comment_status' => $post->comment_status,
        'ping_status'    => $post->ping_status,
        'post_author'    => $new_post_author,
        'post_content'   => cirtolat($post->post_content),
        'post_excerpt'   => cirtolat($post->post_excerpt),
        'post_name'      => $post->post_name,
        'post_parent'    => $post->post_parent,
        'post_password'  => $post->post_password,
        'post_status'    => 'draft',
        'post_title'     => cirtolat($post->post_title),
        'post_type'      => $post->post_type,
        'to_ping'        => $post->to_ping,
        'menu_order'     => $post->menu_order
        );

        /* Insert the post using wp_insert_post() */
        $new_post_id = wp_insert_post( $args );
     
        /* Get all current post terms, then set them against the new draft. */
        $taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
        foreach ($taxonomies as $taxonomy) {
            $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
            $post_terms[0] = 'lat';
            wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
        }
        /* Duplicate all of the post metadata */
        $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");

        if (count($post_meta_infos)!=0) {
            $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
            foreach ($post_meta_infos as $meta_info) {
                $meta_key = $meta_info->meta_key;

                if( $meta_key == '_wp_old_slug' ) continue;
                $meta_value = addslashes($meta_info->meta_value);
                $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
            }

        $sql_query.= implode(" UNION ALL ", $sql_query_sel);
        $wpdb->query($sql_query);
        }
     
        /* Redirect to the Edit post screen for the new draft */
        wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
        exit;
} else {
        wp_die('Post creation failed, could not find original post: ' . $post_id);
    }
}

add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );
	
/* Add the duplicate link to the action list for post_row_actions */
function rd_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?action=rd_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce' ) . '" title="Duplicate this item" rel="permalink">Преслови</a>';
	}
	return $actions;
}
	
add_filter('post_row_actions', 'rd_duplicate_post_link', 10, 2 );

add_filter('page_row_actions', 'rd_duplicate_post_link', 10, 2);

function cirtolat($str){
    $latin = array("A","B","V","G","D","Đ","E","Ž","Z","I","J","K","L","LJ","M","N","NJ","O","P","R","S","T","Ć","U","F","H","C","Č","DŽ","Š","a","b","v","g","d","đ","e","ž","z","i","j","k","l","lj","m","n","nj","o","p","r","s","t","ć","u","f","h","c","č","dž","š");
    $ciril = array("А","Б","В","Г","Д","Ђ","Е","Ж","З","И","Ј","К","Л","Љ","М","Н","Њ","О","П","Р","С","Т","Ћ","У","Ф","Х","Ц","Ч","Џ","Ш","а","б","в","г","д","ђ","е","ж","з","и","ј","к","л","љ","м","н","њ","о","п","р","с","т","ћ","у","ф","х","ц","ч","џ","ш");
    $pom = "";
    $key;
    $output = "";
    for($i=0; $i < strlen($str); $i++){
        $pom = mb_substr($str,$i,1);
        $key = array_search($pom, $ciril);
        if($key == null){
            $output .= $pom;  
        }
        else $output .= $latin[$key];
    }
    return $output;
}
?>