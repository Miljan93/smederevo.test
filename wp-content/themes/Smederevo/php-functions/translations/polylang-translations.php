<?php

/* Generisanje skraćenog koda */
function polylang_shortcode() {
	ob_start();
	pll_the_languages(array('show_flags'=>0,'show_names'=>1));
	$flags = ob_get_clean();
	return $flags;
}
add_shortcode( 'polylang', 'polylang_shortcode' );
/* Generisanje skraćenog koda - kraj*/

/* Registraovanje statičkih stringova */
pll_register_string('smederevo', 'Становници', 'Корисник');
pll_register_string('smederevo', 'Становници - Стране', 'Корисник');
pll_register_string('smederevo', 'Становници - Сервиси', 'Корисник');
pll_register_string('smederevo', 'Становници - Вести', 'Корисник');
pll_register_string('smederevo', 'Становници - Обавештења', 'Корисник');
pll_register_string('smederevo', 'Ваш град мисли на вас', 'Корисник');

pll_register_string('smederevo', 'Посетиоци', 'Корисник');
pll_register_string('smederevo', 'Посетиоци - Стране', 'Корисник');
pll_register_string('smederevo', 'Посетиоци - Сервиси', 'Корисник');
pll_register_string('smederevo', 'Посетиоци - Вести', 'Корисник');
pll_register_string('smederevo', 'Посетиоци - Обавештења', 'Корисник');
pll_register_string('smederevo', 'Упознајте Смедерево', 'Корисник');

pll_register_string('smederevo', 'Привредници', 'Корисник');
pll_register_string('smederevo', 'Привредници - Стране', 'Корисник');
pll_register_string('smederevo', 'Привредници - Сервиси', 'Корисник');
pll_register_string('smederevo', 'Привредници - Вести', 'Корисник');
pll_register_string('smederevo', 'Привредници - Обавештења', 'Корисник');


pll_register_string('smederevo', 'Локална самоуправа', 'Корисник');
pll_register_string('smederevo', 'Локална самоуправа - Стране', 'Корисник');
pll_register_string('smederevo', 'Локална самоуправа - Сервиси', 'Корисник');
pll_register_string('smederevo', 'Локална самоуправа - Вести', 'Корисник');
pll_register_string('smederevo', 'Локална самоуправа - Обавештења', 'Корисник');


pll_register_string('smederevo', 'Сервисне информације', 'Корисник');
pll_register_string('smederevo', 'Сервисне информације - Стране', 'Корисник');
pll_register_string('smederevo', 'Сервисне информације - Сервиси', 'Корисник');
pll_register_string('smederevo', 'Сервисне информације - Вести', 'Корисник');
pll_register_string('smederevo', 'Сервисне информације - Обавештења', 'Корисник');

pll_register_string('smederevo', 'Контакт стране', 'Корисник');

pll_register_string('smederevo', 'Најпосећеније странe', 'Корисник');
pll_register_string('smederevo', 'Добро дошли у Смедерево', 'Корисник');
pll_register_string('smederevo', 'Како вам можемо помоћи?', 'Корисник');
pll_register_string('smederevo', 'Упишите термин претраге...', 'Корисник');
pll_register_string('smederevo', 'Истакнуте стране', 'Корисник');

pll_register_string('smederevo', 'Све стране', 'Корисник');
pll_register_string('smederevo', 'Не постоје овакве стране.', 'Корисник');
pll_register_string('smederevo', 'Не постоје овакве стране у овом одељку.', 'Корисник');

pll_register_string('smederevo', 'Вести', 'Корисник');
pll_register_string('smederevo', 'Не постоје истакнуте вести на страни.', 'Корисник');
pll_register_string('smederevo', 'Не постоје вести у овом одељку.', 'Корисник');
pll_register_string('smederevo', 'Погледај архиву вести', 'Корисник');
pll_register_string('smederevo', 'Погледај архиву свих вести', 'Корисник');
pll_register_string('smederevo', 'Архива свих вести', 'Корисник');

pll_register_string('smederevo', 'Обавештења', 'Корисник');
pll_register_string('smederevo', 'Не постоје истакнута обавештења на страници.', 'Корисник');
pll_register_string('smederevo', 'Не постоје обавештења у овом одељку.', 'Корисник');
pll_register_string('smederevo', 'Погледај архиву обавештења', 'Корисник');
pll_register_string('smederevo', 'Погледај архиву свих обавештења', 'Корисник');
pll_register_string('smederevo', 'Архива свих обавештења', 'Корисник');

pll_register_string('smederevo', 'Сви сервиси', 'Корисник');
pll_register_string('smederevo', 'Истакнути сервиси', 'Корисник');
pll_register_string('smederevo', 'Не постоје овакви сервиси.', 'Корисник');
pll_register_string('smederevo', 'Не постоје овакви сервиси у овом одељку.', 'Корисник');

pll_register_string('smederevo', 'Не постоје питања грађана.', 'Корисник');
pll_register_string('smederevo', 'Не постоје питања грађана у овом одељку.', 'Корисник');

pll_register_string('smederevo', 'Објављено:', 'Корисник');
pll_register_string('smederevo', '« Претходна страна', 'Корисник');

pll_register_string('smederevo', 'Резултати претраге за', 'Корисник');
pll_register_string('smederevo', 'Филтрирање резултата', 'Корисник');
pll_register_string('smederevo', 'Преглед свих резултата', 'Корисник');

pll_register_string('smederevo', 'Одговори на питања', 'Корисник');
pll_register_string('smederevo', 'Сервиси', 'Корисник');
pll_register_string('smederevo', 'Поставите питање', 'Корисник');
pll_register_string('smederevo', 'Одабери тип питања', 'Корисник');
pll_register_string('smederevo', 'Пронађи питање', 'Корисник');



/* Registraovanje statičkih stringova - kraj*/

?>