<?php
function ip_search_filter_item_class($passed_string = false) {
	$post_type = (isset($_GET['post_type']) ? $_GET['post_type'] : false);

	if($passed_string == $post_type) {
		echo 'current';
	}
}

function ip_search_filter($query) {
	// Check we're not in admin area
	if(!is_admin()) {
		// Check if this is the main search query
		if($query->is_main_query() && $query->is_search()) {
			// Check if $_GET['post_type'] is set
			if(isset($_GET['post_type']) && $_GET['post_type'] != '') {
				// Filter it just to be safe
				$post_type = sanitize_text_field($_GET['post_type']);

				// Set the post type
				$query->set('post_type', $post_type);
			}
		}
	}

	// Return query
	return $query;
}

add_filter('pre_get_posts', 'ip_search_filter');


function SearchFilter($query) {
	$post_type = $_GET['post_type'];
	if (!$post_type) {
		$post_type = 'any';
	}
	if ($query->is_search) {
		$query->set('post_type', $post_type);
	};
	return $query;
} 
?>