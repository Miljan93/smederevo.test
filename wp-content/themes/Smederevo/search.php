<style>

    #results-wrapper{
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: space-between;
        align-items: center;
        margin-top: 120px;
    }
    #results-wrapper section:nth-child(1){
        order: 2;
    }
    #results-wrapper section:nth-child(2){
        order: 1;
    }
    .result{
        margin-bottom: 2rem;
    }
    .result-date{
        color: #808080;
    }
    #search-filter{
        padding: 10px;
        border: 1px solid #808080;
    }
    #search-filter li{
        list-style-type: none;
        margin-bottom: 10px;
    }
    #search-filter li a{
        text-decoration: none;
    }

    @media only screen and (min-width: 1290px) {
        header{
            display: none;
        }
        #results-container{
            margin-top: 160px;
        }
        #results-container h1{
            text-align: center;
        }
        #results-wrapper{
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: flex-start;
        }
        #results-wrapper section{
            width: 400px;
        }
        #results-wrapper section:nth-child(1){
            order: 1;
        }
        #results-wrapper section:nth-child(2){
            order: 2;
        } 
    }
</style>
<?php 
get_header(); ?>
<?php get_template_part('partials/mobile-header'); ?>
<div class="container">
    <div id="results-container">
        <h1 class="result-title" itemprop="name"><?php printf( esc_html__( 'Резултати претраге за: %s', 'Smederevo' ), get_search_query() ); ?></h1>
        <?php get_search_form(); ?>
            <div id="results-wrapper">
                <section id="results-section">
                    <?php
                    if(have_posts()):
                        while (have_posts()) : the_post();?>
                        <div class="result">
                            <?php if ( get_post_type() === 'stanovnici' ):?> 
                                <img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="Slika strane">
                            <?php else: ?>
                                <div class="result-picture" style="background: url('<?php the_post_thumbnail_url('medium_large'); ?>');"></div>
                            <?php endif; ?>
                            <a href="<?php the_permalink() ?>"><h3><?php the_title(); ?></h3></a>
                            <span class="result-date"><?php echo pll_e('Објављено: ') ?><?php echo get_the_date(); ?></span>
                            <div><?php the_excerpt() ?></div>
                        </div>
                        <?php 
                        endwhile;
                        else:?>
                            <h2 class="entry-title" itemprop="name"><?php esc_html_e( 'Не постоје резултати за тражени појам.', 'Smederevo' ); ?></h2>
                            <p><?php esc_html_e( 'Пробајте претрагу по другом појму.', 'Smederevo' ); ?></p>  
                        <?php endif;
                    wp_reset_postdata();
                    ?> 
                </section>
                <section id="filter-section">
                    <ul id="search-filter">
                    <h3><?php echo pll_e('Филтрирање резултата') ?></h3>
                        <li>
                            <a class="<?php echo (!isset($_GET['post_type']) ? 'current' : false); ?>" href="<?php echo home_url(); ?>/?s=<?php echo get_search_query(); ?>">
                            <?php echo pll_e('Преглед свих резултата') ?>
                        </a>
                    </li>
                    <?php $args=array(
                        'public'                => true,
                        'exclude_from_search'   => false,
                        '_builtin'              => false
                        ); 

                        $output = 'objects'; // names or objects, note names is the default
                        $operator = 'and'; // 'and' or 'or'
                        $post_types = get_post_types($args,$output,$operator);
                        foreach ($post_types as $post_type ):?>
                            <li>
                                <label for="">
                                    <a class="<?php ip_search_filter_item_class("$post_type->name"); ?>" href="<?php echo home_url(); ?>/?s=<?php echo get_search_query(); ?>&post_type=<?php echo $post_type->name ?>">
                                        <?php echo pll_e($post_type->label) ?>
                                    </a>
                                </label>
                            </li>
                        <?php
                        endforeach;
                    ?>
                    </ul>
                </section>
            </div>
        <div id="pagination-wrapper">
            <?php echo paginate_links();?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
