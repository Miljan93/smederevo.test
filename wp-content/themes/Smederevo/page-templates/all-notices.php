
<?php 
/*
Template Name: Архива свих најава догађаја
*/
get_header() ?>
<?php get_template_part('partials/mobile-header'); ?>
<div class="margin-top container">
    <div class="row">
        <?php get_template_part( 'sidebar', '', $args ); ?>
        <main class="col-lg-8">
            <h1 class="text-center"><?php echo pll_e('Архива свих најава догађаја'); ?></h1>
            <section>
                <div class="search-results-wrapper">
                <?php
                $args = array(
                    'post_type' => array('s-najave-dogadjaja', 'po-najave-dogadjaja', 'pr-najave-dogadjaja', 'ls-najave-dogadjaja', 'si-najave-dogadjaja'),
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'paged' => $paged,
                );?>
                <?php get_template_part( 'partials/notices-archive', '', $args ); ?>
                </div>
            </section>
        </main>
    </div>
</div>
<?php get_footer() ?>