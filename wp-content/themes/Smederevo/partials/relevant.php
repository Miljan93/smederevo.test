<?php 
$query = new WP_Query( $args );
if($query -> have_posts()):?>
    <ul class="relevant">
        <?php while ($query -> have_posts()) : $query -> the_post();?>
        <li>
            <a href="<?php the_permalink() ?>"><span><?php the_title(); ?></span></a>
        </li>
        <?php endwhile;?>
    </ul>
    <?php else: ?>
    <p class="text-center"><?php echo pll_e('Не постоје релевантне објаве.')?></p>
<?php endif;
wp_reset_postdata();
?>