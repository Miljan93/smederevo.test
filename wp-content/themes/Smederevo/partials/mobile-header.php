<header>
    <div class="emblem-container-mobile">
        <a href="<?php echo esc_url( home_url('/') ); ?>">
            <div class="emblem-wrapper-mobile">
                <img class="emblem-mobile" src="<?php site_icon_url(); ?>" alt="Grb-Smederevo">
                <div class="emblem-text-wrapper-mobile">
                    <p><?php bloginfo( 'name' ) ?></p>
                    <hr>
                    <span><?php bloginfo( 'description' ) ?></span>
                </div>
            </div>
        </a>
    </div>
</header>