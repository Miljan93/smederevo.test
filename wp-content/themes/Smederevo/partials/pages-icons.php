<?php 
$query = new WP_Query( $args );
    if ( $query->have_posts() ): ?> 
        <div class="pages-wrapper">
            <?php while ( $query->have_posts() ): $query->the_post();?>
                <?php //var_dump( get_post_type( $post->ID )); die; ?>
                <?php $externalLink = get_post_meta($post->ID, '_external-link', true); ?>
                <?php if(!empty($externalLink)): ?>
                    <a class="page-button-wrapper <?php echo 'outline-'.get_post_type( $post->ID ).'' ?>" href="<?php echo $externalLink ?>">
                <?php else: ?>
                    <a class="page-button-wrapper <?php echo 'outline-'.get_post_type( $post->ID ).'' ?>" href="<?php the_permalink() ?>">
                <?php endif; ?>
                    <div class="page-custom">
                        <?php if( has_post_thumbnail() ):?>
                            <img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="Slika strane">
                        <?php else: ?>
                            <img src="<?php echo get_template_directory_uri()?>/assets/icons/Document.png" alt="Slika strane">
                        <?php endif ?>
                        <p><?php the_title(); ?></p>
                    </div>
                </a>
            <?php endwhile;?>
        </div>
    <?php 
    else: 
        if(is_front_page()):?>
            <p class="text-center"><?php echo pll_e('Не постоје овакве стране.')?></p>
        <?php else:?>
            <p class="text-center"><?php echo pll_e('Не постоје овакве стране у овом одељку.')?></p>
        <?php endif;?>
    <?php endif;
wp_reset_postdata();?>