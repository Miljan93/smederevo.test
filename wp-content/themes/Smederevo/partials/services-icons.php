<?php 
$query = new WP_Query( $args );
    if ( $query->have_posts() ): ?> 
        <dl class="services-wrapper row">
            <?php while ( $query->have_posts() ): $query->the_post();?>
                <?php $externalLink = get_post_meta($post->ID, '_external-link', true); ?>
                <div class="service-wrapper col-sm-6 col-md-4">
                    <div class="service-text-wrapper">
                        <div class="service-image <?php echo 'outline-'.get_post_type( $post->ID ).'' ?>">
                            <?php if( has_post_thumbnail() ):?>
                                <img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="Slika strane">
                            <?php else: ?>
                                <img src="<?php echo get_template_directory_uri()?>/assets/icons/Services.png" alt="Slika strane">
                            <?php endif ?>
                        </div>
                        <p><?php the_title(); ?></p>
                    </div>
                    <?php $args = array(
                    'post_parent'    => $post->ID);
                    $attachments = get_children( $args ); 
                    if ( $attachments ):?>
                    <ul class="service-children">
                    <?php foreach ( $attachments as $attachment ):?>
                        <li class="service-child"><a href="<?php echo get_permalink( $id ) ?>"><?php echo $attachment->post_title ?></a></li>
                    <?php endforeach;?>
                    </ul>
                    <?php endif;?>
                </div>
            <?php endwhile;?>
        </dl>
    <?php 
    else: 
        if(is_front_page()):?>
            <p class="text-center"><?php echo pll_e('Не постоје овакви сервиси.')?></p>
        <?php else:?>
            <p class="text-center"><?php echo pll_e('Не постоје овакви сервиси у овом одељку.')?></p>
        <?php endif;?>
    <?php endif;
wp_reset_postdata();?>