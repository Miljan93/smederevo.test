<?php if(!is_front_page()): ?>
<div id="breadcrumbs-wrapper">
    <div class="container">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<span id="breadcrumbs">','</span>' );
        }
        ?>
    </div>
</div>
<?php endif; ?>
<nav id="navigation" role="navigation">
    <a href="<?php echo esc_url( home_url('/') ); ?>" class="emblem-wrapper" >
        <img class="emblem" src="<?php site_icon_url(); ?>" alt="Grb-Smederevo">
        <div class="emblem-text-wrapper">
            <p><?php bloginfo( 'description' ) ?></p>
            <hr>
            <span><?php bloginfo( 'name' ) ?></span>
        </div>
    </a>
    <div class="container">
        <div id="menu-wrapper">
            <input onclick="uncheckRadioButton()" type="checkbox"/>
            <span></span>
            <span></span>
            <span></span>
            <label for="deselect" id="menu-container">
                <input id="deselect" class="toggleBox" type="radio" name="toggle">
                <?php
                    $menus = get_registered_nav_menus();
                    foreach($menus as $key => $value):?>
                        <a href="<?php echo get_post_type_archive_link( $key ) ?>" class="menu-item-container" >
                            <label for="<?php echo $key ?>" class="subMenu <?php echo 'link-'.$key.'' ?>">
                                <?php echo pll_e($value)?>    
                            </label>
                        </a>
                        <!--<input class="toggleBox" type="radio" name="toggle" id="<?php/* echo $key */?>">
                        <?php /*wp_nav_menu( array( 
                            'theme_location' => $key,
                            'menu_class'     => 'subMenu-content',
                            'container'      => false,
                            ));*/?>-->
                    <?php endforeach;
                ?>
            </label>
        </div>
    </div>
    <div id="language-select-container">
        <a href="javascript:void(0);" onclick="toggleElement(this)">
            <img id="language-select-image" src="<?php echo get_template_directory_uri(); ?>/assets/icons/Language.png" alt="Font-size">
        </a>
        <ul id="language-select-controls" for="language-select-checkbox">
            <?php echo do_shortcode( '[polylang]' ) ?>
        </ul>
    </div>
    <div id="font-size-container">
        <a href="javascript:void(0);" id="toggleFontSize" onclick="toggleElement(this)">
            <img id="font-size" src="<?php echo get_template_directory_uri(); ?>/assets/icons/Font.png" alt="Font-size">
        </a>
        <ul id="font-size-controls">
            <li><a href="javascript:void(0);" onclick="enlargeFont()" class="control-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/Plus.png" alt="Font-Size-Plus"></a></li>
            <li><a href="javascript:void(0);" onclick="resetFont()" class="control-wrapper"><img src="<?php echo get_template_directory_uri(); ?>/assets/icons/Reset.png" alt="Font-Size-Reset"></a></li>
            <li><a href="javascript:void(0);" onclick="diminishFont()" class="control-wrapper"> <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/Minus.png" alt="Font-Size-Minus"></a></li>    
        </ul>
    </div>
</nav>

