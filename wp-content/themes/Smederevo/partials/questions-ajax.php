<?php
$posts_array = '';
$current_post_type = $wp_query->query['post_type'];
if(is_home()):
$posts_array = '["s-pitanja","po-pitanja","pr-pitanja","ls-pitanja","g-pitanja"]';
else:
  if($current_post_type == 'stanovnici'){
    $posts_array = '["s-pitanja"]';
  } 
  if($current_post_type == 'posetioci'){
    $posts_array = '["po-pitanja"]';
  }
  if($current_post_type == 'privrednici'){
    $posts_array = '["pr-pitanja"]';
  }
  if($current_post_type == 'lokalna-samouprava'){
    $posts_array = '["ls-pitanja"]';
  }
  if($current_post_type == 'servisne-informacije'){
    $posts_array = '["g-pitanja"]';
  }
endif;
?>
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <a 
    class="nav-link active" 
    id="questions-fields-tab" 
    data-bs-toggle="tab" 
    href="#questions-fields" 
    role="tab" 
    aria-controls="questions-fields" 
    aria-selected="true"
    data-url="<?php echo admin_url( 'admin-ajax.php' )?>"
    data-post-types = <?php echo $posts_array ?>>
    <i class="far fa-question-circle fa-2x"></i><span class="tab-title"><?php echo pll_e("Одговори на питања") ?></span>
    </a>
  </li>
  <li class="nav-item" role="presentation">
    <a 
    class="nav-link" 
    id="profile-tab" 
    data-bs-toggle="tab" 
    href="#profile" 
    role="tab" 
    aria-controls="profile" 
    aria-selected="false">
    <i class="fas fa-concierge-bell fa-2x"></i><span class="tab-title"><?php echo pll_e("Сервиси") ?></a></span>
  </li>
  <li class="nav-item" role="presentation">
    <a 
    class="nav-link" 
    id="contact-tab" 
    data-bs-toggle="tab" 
    href="#contact" 
    role="tab" 
    aria-controls="contact" 
    aria-selected="false">
    <i class="far fa-envelope fa-2x"></i><span class="tab-title"><?php echo pll_e("Поставите питање") ?></a></span>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div id="spinner"><i class="fas fa-sync-alt fa-4x"></i></div>
  <div class="tab-pane fade show active" id="questions-fields" role="tabpanel" aria-labelledby="questions-fields-tab">
    <div class="tab-breadcrumbs"><span class='tab-breadcrumbs-actions'><i class='fas fa-grip-horizontal'></i></span><span class='tab-breadcrumbs-text'><?php pll_e('Одабери тип питања') ?></span></div>
    <div id="questions-fields-content" class="tab-inner-content"></div>
  </div>
  <div class="tab-pane fade" id="answers" role="tabpanel" aria-labelledby="answers-tab">
    <div class="tab-breadcrumbs"><span id="" class='tab-breadcrumbs-actions'><a href="javascript:void(0);" id="back-to-questions-fields"><i class='fas fa-grip-horizontal'></i><span><?php pll_e('Одабери тип питања') ?></span></a></span><span class='tab-breadcrumbs-text'>&nbsp;/&nbsp;<?php pll_e('Пронађи питање') ?></span></div>
    <div id="answers-content" class="tab-inner-content"></div>
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="tab-breadcrumbs"><span id="" class='tab-breadcrumbs-actions'><i class="fas fa-concierge-bell"></i></i></span><span class='tab-breadcrumbs-text'>/&nbsp;<?php pll_e('Одабери сервис') ?></span></div>
    <div id="profile-content" class="tab-inner-content"></div>
  </div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
    <div class="tab-breadcrumbs"><span id="" class='tab-breadcrumbs-actions'><i class="far fa-paper-plane"></i></span><span class='tab-breadcrumbs-text'>/&nbsp;<?php pll_e('Постави питање') ?></span></div>
    <div id="contact-content" class="tab-inner-content"><?php echo do_shortcode('[wpforms id="8"]') ?></div>
  </div>
</div>



