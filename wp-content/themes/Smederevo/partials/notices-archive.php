<?php
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$query = new WP_Query( $args );
    if($query -> have_posts()):
        while ($query -> have_posts()) : $query -> the_post();?>
        <div class="row <?php echo 'border-'.get_post_type( $post->ID ).'' ?>">
            <?php if ( has_post_thumbnail() ):?>
                <div class="notices-picture col-sm-4 order-md-last" style="background: url('<?php the_post_thumbnail_url('medium_large'); ?>');"></div>
            <?php endif ?>
            <div class="notices col-sm-8">
                <a href="<?php the_permalink() ?>"><h2><?php the_title(); ?></h2></a>
                <p class="notices-date"><?php echo pll_e('Објављено: ')?><?php echo get_the_date(); ?></p>
                <div><?php the_excerpt() ?></div>
            </div>
        </div>
        <?php 
        endwhile;?>
        <div id='pagination-wrapper'>
            <?php echo paginate_links(array('total'=>$query->max_num_pages)); ?>
        </div>
    <?php endif;
wp_reset_postdata();
?>