<div class="aside-wrapper">
    <a class="button" href="javascript:window.history.back();"><?php echo pll_e('Претходна страна')?></a>
    <?php
    global $post;
    $post_slug = $post->post_name;
    if($post_slug == "arhiva-svih-vesti"):?>
        <h3 class="text-center"><?php pll_e('Филтрирање вести') ?></h3>
    <?php endif;
    if($post_slug == "arhiva-svih-obavestenja"):?>
        <h3 class="text-center"><?php pll_e('Филтрирање обавештења') ?></h3>
    <?php endif;
    ?>
</div>