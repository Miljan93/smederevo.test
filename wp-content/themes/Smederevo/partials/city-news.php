<div class="city-news-container">
    <h2><?php echo pll_e('Градске актуелности')?></h2>
    <div class="city-news-wrapper">
        <?php
        $query = new WP_Query( $args );
        if($query -> have_posts()):
            while ($query -> have_posts()) : $query -> the_post();?>
            <div class="city-news <?php echo 'border-'.get_post_type( $post->ID ).'' ?>">
                <a href="<?php the_permalink() ?>">
                    <?php if ( $query->current_post == 0 && has_post_thumbnail( $query->current_post ) ):?>
                        <div class="city-news-picture" style="background: url('<?php the_post_thumbnail_url('medium_large'); ?>');"></div>
                    <?php endif ?>
                    <h3><?php the_title(); ?></h3>
                </a>
                <span class="city-news-date"><?php echo pll_e('Објављено:')?> <?php echo get_the_date(); ?></span>
                <?php if ( $query->current_post == 0 ):?>
                    <div><?php the_excerpt() ?></div>
                <?php endif ?>
            </div>
            <?php 
            endwhile;?>

            <?php if(!is_front_page()):?>
                <div class="button-wrapper">
                    <a class="button" href="<?php echo get_post_type_archive_link( get_post_type() ) ?>"><?php echo pll_e('Погледај архиву градских актуелности')?></a>
                </div>
            <?php else:?>
                <div class="button-wrapper">
                    <a class="button" href="<?php echo site_url('arhiva-svih-gradskih-aktuelnosti'); ?>"><?php echo pll_e('Погледај архиву свих градских актуелности')?></a>
                </div>
            <?php endif; ?>
        <?php else: 
            if(is_front_page()):?>
                <p class="text-center"><?php echo pll_e('Не постоје истакнута Градске актуелности на страни.')?></p>
            <?php else:?>
                <p class="text-center"><?php echo pll_e('Не постоје Градске актуелности у овом одељку.')?></p>
            <?php endif;?>
        <?php endif;
        wp_reset_postdata();?>
    </div>
</div>