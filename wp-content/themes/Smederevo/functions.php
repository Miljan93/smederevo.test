<?php
add_action( 'after_setup_theme', 'smederevo_setup' );
function smederevo_setup() {
	load_theme_textdomain( 'smederevo', get_template_directory() . '/languages' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails', array('stanovnici', 's-vesti', 's-obavestenja', 's-servisi', 's-pitanja',
	'posetioci', 'po-vesti', 'po-obavestenja','po-servisi', 'po-pitanja',
	'privrednici', 'pr-vesti', 'pr-obavestenja','pr-servisi', 'pr-pitanja',
	'lokalna-samouprava', 'ls-vesti', 'ls-obavestenja','ls-servisi', 'ls-pitanja',
	'servisne-informacije', 'si-vesti', 'si-obavestenja', 'si-servisi', 'si-pitanja'));
	add_theme_support( 'responsive-embeds' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'html5', array( 'search-form' ) );
	add_theme_support( 'page-attributes', array( 's-servisi', 'po-servisi', 'pr-servisi', 'gu-servisi', 'si-servisi' ) );
	global $content_width;
	if ( !isset( $content_width ) ) { $content_width = 1920; }
}

add_action( 'wp_footer', 'smederevo_footer' );
function smederevo_footer() {
?>
<script>
jQuery(document).ready(function($) {
var deviceAgent = navigator.userAgent.toLowerCase();
if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
$("html").addClass("ios");
}
if (navigator.userAgent.search("MSIE") >= 0) {
$("html").addClass("ie");
}
else if (navigator.userAgent.search("Chrome") >= 0) {
$("html").addClass("chrome");
}
else if (navigator.userAgent.search("Firefox") >= 0) {
$("html").addClass("firefox");
}
else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
$("html").addClass("safari");
}
else if (navigator.userAgent.search("Opera") >= 0) {
$("html").addClass("opera");
}
});
</script>
<?php
}
add_filter( 'document_title_separator', 'smederevo_document_title_separator' );
function smederevo_document_title_separator( $sep ) {
$sep = '|';
return $sep;
}
//add_filter( 'use_block_editor_for_post', '__return_false' );
add_filter( 'show_admin_bar', '__return_false' );
add_filter( 'the_title', 'smederevo_title' );
function smederevo_title( $title ) {
if ( $title == '' ) {
return '...';
} else {
return $title;
}
}
add_filter( 'nav_menu_link_attributes', 'smederevo_schema_url', 10 );
function smederevo_schema_url( $atts ) {
$atts['itemprop'] = 'url';
return $atts;
}
if ( !function_exists( 'smederevo_wp_body_open' ) ) {
function smederevo_wp_body_open() {
do_action( 'wp_body_open' );
}
}
add_action( 'wp_body_open', 'smederevo_skip_link', 5 );
function smederevo_skip_link() {
echo '<a href="main" class="skip-link screen-reader-text">' . esc_html__( 'Пређи на садржај', 'smederevo' ) . '</a>';
}
add_filter( 'the_content_more_link', 'smederevo_read_more_link' );
function smederevo_read_more_link() {
if ( !is_admin() ) {
return ' <a href="' . esc_url( get_permalink() ) . '" class="more-link">' . sprintf( __( 'Прочитај више...', 'smederevo' ), '<span class="screen-reader-text">  ' . esc_html( get_the_title() ) . '</span>' ) . '</a>';
}
}
add_filter( 'excerpt_more', 'smederevo_excerpt_read_more_link' );
function smederevo_excerpt_read_more_link( $more ) {
if ( !is_admin() ) {
global $post;
return ' <a href="' . esc_url( get_permalink( $post->ID ) ) . '" class="more-link">' . sprintf( __( 'Прочитај више...', 'smederevo' ), '<span class="screen-reader-text">  ' . esc_html( get_the_title() ) . '</span>' ) . '</a>';
}
}
add_filter( 'big_image_size_threshold', '__return_false' );
add_filter( 'intermediate_image_sizes_advanced', 'smederevo_image_insert_override' );
function smederevo_image_insert_override( $sizes ) {
unset( $sizes['medium_large'] );
unset( $sizes['1536x1536'] );
unset( $sizes['2048x2048'] );
return $sizes;
}
add_action( 'widgets_init', 'smederevo_widgets_init' );
function smederevo_widgets_init() {
register_sidebar( array(
'name' => esc_html__( 'Sidebar Widget Area', 'smederevo' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => '</li>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}

add_filter('get_image_tag_class','wpse302130_add_image_class');

function wpse302130_add_image_class ($class){
    $class .= ' img-fluid';
    return $class;
}

/*
*   REGISTRACIJA MENIJA
*/

require_once dirname( __FILE__ ) . '/php-functions/frontend-menus/frontend-menus-registration.php';

/*
*   REGISTRACIJA MENIJA - KRAJ
*/

/*
* PRILAGOĐENI TIPOVI ČLANAKA 
*/

/* Dodavanje menija u admin panel */

require_once dirname( __FILE__ ) . '/php-functions/admin-menu/admin-menu.php';

/* Dodavanje menija u admin panel - KRAJ */

/* Registrovanje prilagođenog tipa članaka */

require_once dirname( __FILE__ ) . '/php-functions/custom-post-types/cpt-stanovnici.php';
require_once dirname( __FILE__ ) . '/php-functions/custom-post-types/cpt-posetioci.php';
require_once dirname( __FILE__ ) . '/php-functions/custom-post-types/cpt-privrednici.php';
require_once dirname( __FILE__ ) . '/php-functions/custom-post-types/cpt-lokalna_samouprava.php';
require_once dirname( __FILE__ ) . '/php-functions/custom-post-types/cpt-servisne_informacije.php';

/* Registrovanje prilagođenog tipa članaka - KRAJ */

/* Dodatne opcije prilagođenog tipa članaka */

require_once dirname( __FILE__ ) . '/php-functions/custom-post-types/cpt-views-count.php';
require_once dirname( __FILE__ ) . '/php-functions/custom-post-types/cpt-meta.php';

/* Dodatne opcije prilagođenog tipa članaka - KRAJ */

/*
* PRILAGOĐENI TIPOVI ČLANAKA - KRAJ
*/


/*
*   PODEŠAVANJA PERFORMANSI
*/

require_once dirname( __FILE__ ) . '/php-functions/website-performance/speed-performance.php';

/*
*   PODEŠAVANJA PERFORMANSI - KRAJ
*/

/*
* KONTROLA PRISTUPA PUTEM ROLA
*/

require_once dirname( __FILE__ ) . '/php-functions/permissions/roles-permissions.php';

/*
* KONTROLA PRISTUPA PUTEM ROLA - KRAJ
*/

/*
* FILTRIRANJA NA STRANI REZULTATA PRETRAGE
*/

require_once dirname( __FILE__ ) . '/php-functions/search/custom-search-filter.php';

/*
* FILTRIRANJA NA STRANI REZULTATA PRETRAGE - KRAJ
*/

/*
* LOCIRANJE TEMPLEJTA PO FOLDERIMA
*/

require_once dirname( __FILE__ ) . '/php-functions/templates/templates-redirection.php';

/*
* LOCIRANJE TEMPLEJTA PO FOLDERIMA - KRAJ
*/

/*
* POLYLANG
*/

require_once dirname( __FILE__ ) . '/php-functions/translations/polylang-translations.php';

/*
* POLYLANG END
*/

/*
* PAGE TRANSLATION
*/

require_once dirname( __FILE__ ) . '/php-functions/translations/automatic-page-translation.php';

/*
* PAGE TRANSLATION END
*/

/* 
*	SEO OPTIMIZACIJA SAMO ZA ADMINA
*/

require_once dirname( __FILE__ ) . '/php-functions/website-performance/seo-performance.php';

/* 
*	SEO OPTIMIZACIJA SAMO ZA ADMINA - END
*/

/*
*	POZIVI AJAX FUNKCIJA
*/

require_once dirname( __FILE__ ) . '/ajax-functions/guestions-fields-load.php';
require_once dirname( __FILE__ ) . '/ajax-functions/answers-load.php';

/*
*	POZIVI AJAX FUNKCIJA - KRAJ
*/
