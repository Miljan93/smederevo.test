<style>
    .link-lokalna-samouprava{
        box-shadow: inset 0 -2px 0 var(--si-color);
    }
</style>
<?php get_header(); ?>
<?php get_template_part('partials/mobile-header'); ?>
<div class="margin-top container">
    <div class="row">
        <?php get_template_part( 'sidebar', '', $args ); ?>
        <main class="col-lg-8">
            <h1 class="text-center"><?php echo pll_e('Архива вести - Градске управе'); ?></h1>
            <section>
                <div class="search-results-wrapper">
                <?php
                $args = array(
                    'post_type' => 'ls-vesti',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'paged' => $paged,
                );?>
                <?php get_template_part( 'partials/news-archive', '', $args ); ?>
                </div>
            </section>
        </main>
    </div>
</div>
<?php get_footer(); ?>
