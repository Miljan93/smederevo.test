<style>
    .link-posetioci{
        box-shadow: inset 0 -2px 0 var(--po-color);
    }
    .hero{
        display: flex;
        justify-content: flex-end;
        align-items: center;
        background-size: cover;
        background-position: center;
        height: 500px;
        padding: 0 20px;
        background-image: -webkit-image-set(
            url("<?php echo get_template_directory_uri(); ?>/assets/images/Hero3_mobile.webp") 2x,
            url("<?php echo get_template_directory_uri(); ?>/assets/images/Hero3.webp") 1x);
        background-image: image-set(
            url("<?php echo get_template_directory_uri(); ?>/assets/images/Hero3.webp") 2x,
            url("<?php echo get_template_directory_uri(); ?>/assets/images/Hero3.webp") 1x);
    }
    .hero-search-wrapper{
        position: relative;
        max-width: 400px;
        background: rgba(46, 103, 173, 0.85);
        color: #fff;
        padding: 25px;
    }
    .hero-search-wrapper h1{
        margin: 0 !important;
    }
    .hero-search-wrapper p{
        margin: 10px 0 !important;
    }
    .hero-search-wrapper form{
        margin: 0 !important;
    }
</style>
<?php get_header(); ?>
<?php get_template_part('partials/mobile-header'); ?>
    <section class="hero">
        <div class="container">
            <div class="hero-search-wrapper">
                <h1><?php echo pll_e('Посетиоци')?></h1>
                <p><?php echo pll_e('Упознајте Смедерево')?></p>
                <?php get_search_form(); ?>
            </div>
        </div>
    </section>
    <main class="archive-page">
        <div class="container">
            <section class="pages-section">
                <h2 class="text-center"><?php echo pll_e('Најпосећеније странe')?></h2>
                <?php
                    $args = array(
                        'post_type' => 'posetioci',
                        'post_status' => 'publish',
                        'meta_key'  => '_views_count', // set custom meta key
                        'orderby'    => 'meta_value_num',
                        'order'      => 'DESC',
                        'posts_per_page' => 4
                    );
                    get_template_part( 'partials/pages-icons', '',$args);
                ?>       
            </section>
            <section class="services-section">
                <h2 class="text-center"><?php echo pll_e('Најпосећенији сервиси')?></h2>
                <?php
                    $args = array(
                        'post_type' => 'po-servisi',
                        'post_status' => 'publish',
                        'meta_key'  => '_views_count', // set custom meta key
                        'orderby'    => 'meta_value_num',
                        'order'      => 'DESC',
                        'posts_per_page' => 4
                    );
                    get_template_part( 'partials/questions-ajax', '',$args);
                ?>       
            </section>
            <section class="news-notices-wrapper">
                <?php 
                    $args = array(
                        'post_type' => 'po-vesti',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'posts_per_page' => 5
                    );
                    get_template_part( 'partials/news', '', $args );
                    $args = array(
                        'post_type' => 'po-najave-dogadjaja',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'posts_per_page' => 5
                    );
                    get_template_part( 'partials/notices', '', $args );
                    $args = array(
                        'post_type' => 'po-g-aktuelnosti',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'posts_per_page' => 5
                    );
                    get_template_part( 'partials/city-news', '', $args );
                ?>
            </section>
            <section class="pages-section">
                <h2 class="text-center"><?php echo pll_e('Све стране')?></h2>
                <?php
                $args = array(
                    'post_type' => 'posetioci',
                    'post_status' => 'publish',
                    'oderby' => 'date',
                    'order' => 'DESC',
                    'post_per_page' => -1
                );?>
                <?php get_template_part( '/partials/pages-icons', '', $args) ?>   
            </section>
            <!--<section class="services-section">
                <h2 class="text-center"><?php //echo pll_e('Сви сервиси')?></h2>
                <?php
                    /*$args = array(
                        'post_type' => 'po-servisi',
                        'post_status' => 'publish',
                        'oderby' => 'date',
                        'order' => 'DESC',
                        'post_per_page' => -1
                    );
                    get_template_part( 'partials/services-icons', '',$args);*/
                ?>       
            </section>-->
        </div>
    </main> 
<?php get_footer(); ?>