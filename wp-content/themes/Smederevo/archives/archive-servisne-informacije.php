<style>
    .hero {
    background-image: -webkit-image-set(
        url("<?php echo get_template_directory_uri(); ?>/assets/images/Hero5_mobile.webp") 2x,
        url("<?php echo get_template_directory_uri(); ?>/assets/images/Hero5.webp") 1x);
    background-image: image-set(
        url("<?php echo get_template_directory_uri(); ?>/assets/images/Hero5_mobile.webp") 2x,
        url("<?php echo get_template_directory_uri(); ?>/assets/images/Hero5.webp") 1x);
    }
    .link-servisne-informacije{
        box-shadow: inset 0 -2px 0 var(--g-color);
    }
</style>
<?php get_header(); ?>
<?php get_template_part('partials/mobile-header'); ?>
    <!--<section class="hero" style="background-image:url('<?php //echo get_template_directory_uri(); ?>/assets/images/Hero5.webp')">
        <div class="container">
            <div class="hero-search-wrapper">
                <h1><?php //echo pll_e('Локална самоуправа')?></h1>
                <p><?php //echo pll_e('Ваш град мисли на вас')?></p>
                <?php //get_search_form(); ?>
            </div>
        </div>
    </section>-->
    <main class="archive-page margin-top">
        <h1 class="text-center"><?php echo pll_e('Сервисне информације')?></h1>
        <div class="container">
            <section class="pages-section">
                <h2 class="text-center"><?php echo pll_e('Најпосећеније странe')?></h2>
                <?php
                    $args = array(
                        'post_type' => 'servisne-informacije',
                        'post_status' => 'publish',
                        'meta_key'  => '_views_count', // set custom meta key
                        'orderby'    => 'meta_value_num',
                        'order'      => 'DESC',
                        'posts_per_page' => 4
                    );
                    get_template_part( 'partials/pages-icons', '',$args);
                ?>       
            </section>
            <section class="services-section">
                <h2 class="text-center"><?php echo pll_e('Најпосећенији сервиси')?></h2>
                <?php
                    $args = array(
                        'post_type' => 'si-servisi',
                        'post_status' => 'publish',
                        'meta_key'  => '_views_count', // set custom meta key
                        'orderby'    => 'meta_value_num',
                        'order'      => 'DESC',
                        'posts_per_page' => 4
                    );
                    get_template_part( 'partials/questions-ajax', '',$args);
                ?>       
            </section>
            <section class="news-notices-wrapper">
                <?php 
                    $args = array(
                        'post_type' => 'si-vesti',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'posts_per_page' => 5
                    );
                    get_template_part( 'partials/news', '', $args );
                    $args = array(
                        'post_type' => 'si-najave-dogadjaja',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'posts_per_page' => 5
                    );
                    get_template_part( 'partials/notices', '', $args );
                    $args = array(
                        'post_type' => 'si-g-aktuelnosti',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'posts_per_page' => 5
                    );
                    get_template_part( 'partials/city-news', '', $args );
                ?>
            </section>
            <section class="pages-section">
                <h2 class="text-center"><?php echo pll_e('Све стране')?></h2>
                <?php
                $args = array(
                    'post_type' => 'servisne-informacije',
                    'post_status' => 'publish',
                    'oderby' => 'date',
                    'order' => 'DESC',
                    'post_per_page' => -1
                );?>
                <?php get_template_part( '/partials/pages-icons', '', $args) ?>   
            </section>
            <!--<section class="services-section">
                <h2 class="text-center"><?php //echo pll_e('Сви сервиси')?></h2>
                <?php
                    /*$args = array(
                        'post_type' => 'si-servisi',
                        'post_status' => 'publish',
                        'oderby' => 'date',
                        'order' => 'DESC',
                        'post_per_page' => -1
                    );
                    get_template_part( 'partials/services-icons', '',$args);*/
                ?>       
            </section>-->
        </div>
    </main> 
<?php get_footer(); ?>