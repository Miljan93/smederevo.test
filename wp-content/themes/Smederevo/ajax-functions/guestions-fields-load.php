<?php
add_action('wp_ajax_nopriv_load_questions_fields', 'load_questions_fields');
add_action('wp_ajax_load_questions_fields', 'load_questions_fields');

function load_questions_fields(){
	$post_types = $_POST["post_types"];
	
	$post_types_array = array();
	foreach($post_types as $post_type){
		array_push($post_types_array, $post_type);	
	}
	$args = array(
		'post_type' => $post_types_array,
		'posts_per_page' => -1,
		'post_parent' => 0
	);
	$query = new WP_Query($args);
    if ( $query->have_posts() ): ?>
        <dl class="questions-fields row">
            <?php while ( $query->have_posts() ): $query->the_post();?>
				<?php $post_id = get_the_ID();
				$children = get_children( array('post_parent' => get_the_ID()) );
				if ( ! empty($children) ): ?>
				<div class="question-field col-sm-6 col-md-4">
					<a class="question-field-link" href="javascript:void(0);" onclick="toggleElement(this)">
						<dd class="question-field-text-wrapper">
							<span class="question-field-image <?php echo 'outline-'.get_post_type( $post->ID ).'' ?>">
								<?php if( has_post_thumbnail() ):?>
									<img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="Slika strane">
								<?php else: ?>
									<img src="<?php echo get_template_directory_uri()?>/assets/icons/Question.png" alt="Slika strane">
								<?php endif ?>
							</span>
							<span><?php the_title(); ?></span>
						</dd>
					</a>
					<?php foreach($post_types_array as $post_type):?>
						<?php 
						$child_pages = get_pages( array(
						'child_of' 		=> get_the_ID(),
						'parent'		=> get_the_ID(),
						'post_type' 	=> $post_type,
						));?>	
						<?php foreach($child_pages as $child_page): ?>
							<ul class="question-field-children">
								<li class="question-field-child">
									<a	href="javascript:void(0);"
										class = "answers"
										data-page-id = "<?php echo $child_page->ID ?>"
										data-post-type = "<?php echo $post_type ?>"
										data-url="<?php echo admin_url( 'admin-ajax.php' )?>">
									<?php echo $child_page->post_title ?>
									</a>
								</li>
							</ul>
							<?php endforeach; ?>
						<?php endforeach; ?>
				</div>
			<?php	endif;
			endwhile;?>
        </dl>
    <?php 
    else:?>
		<p class="text-center"><?php echo pll_e('Не постоје питања грађана у овом одељку.')?></p>
    <?php endif;
	wp_reset_postdata();
	wp_die();
}
?>