<?php
add_action('wp_ajax_nopriv_load_answers', 'load_answers');
add_action('wp_ajax_load_answers', 'load_answers');

function load_answers(){
	$post_type = $_POST["post_type"];
	$page_id = $_POST["page_id"];?>
	<div class="accordion row" id="accordionExample">
		<div class="col-sm-9">
		<?php $child_pages = get_pages( array(
			'child_of' 		=> $page_id,
			'parent'		=> $page_id,
			'post_type' 	=> $post_type,
			));
			foreach($child_pages as $child):?>
				<div class="accordion-item">
					<div class="accordion-header" id="heading<?php echo $child->ID; ?>">
						<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse<?php echo $child->ID; ?>" aria-expanded="false" aria-controls="collapse<?php echo $child->ID; ?>">
							<?php echo $child->post_title ?>
						</button>
			</div>
					<div id="collapse<?php echo $child->ID; ?>" class="accordion-collapse collapse" aria-labelledby="heading<?php echo $child->ID; ?>" data-bs-parent="#accordionExample">
						<div class="accordion-body">
							<?php echo $child->post_content ?>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="col-sm-3">
			<?php $parents = get_post_ancestors( $child->ID );
    		echo apply_filters( "the_title", get_the_title( end ( $parents ) ) );?>
		</div>
	</div>
	<?php wp_reset_postdata();
	wp_die();
}
?>