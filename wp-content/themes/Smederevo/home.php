<style>
    .hero{
        display: flex;
        justify-content: flex-end;
        align-items: center;
        background-size: cover;
        background-position: center;
        height: 500px;
        padding: 0 20px;
        background-image: -webkit-image-set(
            url("<?php echo get_template_directory_uri(); ?>/assets/images/Hero_mobile.webp") 2x,
            url("<?php echo get_template_directory_uri(); ?>/assets/images/Hero.webp") 1x);
        background-image: image-set(
            url("<?php echo get_template_directory_uri(); ?>/assets/images/Hero.webp") 2x,
            url("<?php echo get_template_directory_uri(); ?>/assets/images/Hero.webp") 1x);
    }
    .hero-search-wrapper{
        position: relative;
        max-width: 400px;
        background: rgba(66, 86, 112, 0.85);
        color: #fff;
        padding: 25px;
    }
    .hero-search-wrapper h1{
        margin: 0 !important;
    }
    .hero-search-wrapper p{
        margin: 10px 0 !important;
    }
    .hero-search-wrapper form{
        margin: 0 !important;
    }
</style>
<?php
get_header(); ?>
    <?php get_template_part('partials/mobile-header'); ?>
    <section class="hero">
        <div class="container">
            <div class="hero-search-wrapper">
                <h1><?php echo pll_e('Добро дошли у Смедерево')?></h1>
                <p><?php echo pll_e('Како вам можемо помоћи?')?></p>
                <?php get_search_form(); ?>
            </div>
        </div>
    </section>
    <main>
        <div class="container">
            <section class="pages-section no-padding">
                <!--<h2 class="text-center"><?php //echo pll_e('Истакнуте стране')?></h2>-->
                <?php
                    $args = array(
                        'post_type' => array('stanovnici', 'posetioci', 'privrednici', 'lokalna-samouprava', 'servisne-informacije'),
                        'post_status' => 'publish',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'meta_key' => '_prominent',
                        'meta_value' => 1
                    );
                    get_template_part( 'partials/pages-icons', '',$args);
                ?>   
            </section>
            <section class="services-section">
                <!--<h2 class="text-center"><?php //echo pll_e('Истакнути сервиси')?></h2>-->
                <?php
                    get_template_part( 'partials/questions-ajax', '',$args);
                ?>       
            </section>
            <section class="news-notices-wrapper">
                <?php 
                    $args = array(
                        'post_type' => array('s-vesti', 'po-vesti', 'pr-vesti', 'ls-vesti', 'si-vesti'),
                        'post_status' => 'publish',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'meta_key' => '_prominent',
                        'meta_value' => 1,
                    );
                    get_template_part( 'partials/news', '', $args );
                    $args = array(
                        'post_type' => array('s-najave-dogadjaja', 'po-najave-dogadjaja', 'pr-najave-dogadjaja', 'ls-najave-dogadjaja', 'si-najave-dogadjaja'),
                        'post_status' => 'publish',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'meta_key' => '_prominent',
                        'meta_value' => 1,
                    );
                    get_template_part( 'partials/notices', '', $args );
                    $args = array(
                        'post_type' => array('s-g-aktuelnosti', 'po-g-aktuelnosti', 'pr-g-aktuelnosti', 'ls-g-aktuelnosti', 'si-g-aktuelnosti'),
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'meta_key' => '_prominent',
                        'meta_value' => 1,
                    );
                    get_template_part( 'partials/city-news', '', $args );
                ?>
            </section>
            <section class="pages-section">
                <h2 class="text-center"><?php echo pll_e('Најпосећеније стране')?></h2>
                <?php
                $args = array(
                    'post_type' => array('stanovnici', 'posetioci', 'privrednici', 'lokalna-samouprava', 'servisne-informacije'),
                    'post_status' => 'publish',
                    'meta_key'  => '_views_count', // set custom meta key
                    'orderby'    => 'meta_value_num',
                    'order'      => 'DESC',
                    'posts_per_page' => 8
                );?>
                <?php get_template_part( '/partials/pages-icons', '', $args) ?>   
            </section>
        </div> <!-- Main Container End -->
    </main> 
<?php get_footer(); ?>