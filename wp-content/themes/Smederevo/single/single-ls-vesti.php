<style>
    .link-lokalna-samouprava{
        box-shadow: inset 0 -2px 0 var(--s-color);
    }
</style>
<?php get_header(); ?>
<?php get_template_part('partials/mobile-header'); ?>
<div class="margin-top container">
    <div class="row">
        <?php get_template_part( 'sidebar', '', $args ); ?>
        <main class="col-lg-8">
            <section>
                <h1><?php the_title() ?></h1>
                <?php the_content() ?>
            </section>
        </main>
    </div>
</div>
<?php get_footer(); ?>