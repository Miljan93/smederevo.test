<style>
aside{
    padding-top: 2rem;
    padding-bottom: 2rem;
    position: relative;
}
.aside-wrapper{
    position: block;
    background-color: var(--grey3);
    padding: 10px;
    width: 100%;
}
.relevant{
    list-style: none;
}
.relevant li a{
    display: block;
    border: 1px solid #000;
    padding: 10px;
    margin-top: -1px;
}
</style>
<aside class="col-lg-4">
    <div class="aside-wrapper">
        <a class="button" href="javascript:window.history.back();"><?php echo pll_e('« Претходна страна')?></a>
    </div>
</aside>