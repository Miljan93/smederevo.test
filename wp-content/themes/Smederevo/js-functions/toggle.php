<script>

    var previousElement;
    function toggleElement(element){
        
        if(jQuery(element).parent().parent().attr('id') == "navigation"){
            jQuery(element).parent().find('ul').slideToggle( 500 );
        }
        else{
            if(previousElement != element){
                jQuery(element).parent().find('ul').slideToggle( 500 );
                jQuery(previousElement).parent().find('ul').slideToggle( 500 );
                previousElement = element;
            }  
        }
          
    }
    function uncheckRadioButton(){
        document.getElementById("deselect").checked = true;
    }
    
</script>