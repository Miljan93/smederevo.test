<script>
let tags = [
    {tag: 'h1', max_size: 28, min_size: 24, original_size: 26},
    {tag: 'h2', max_size: 26, min_size: 22, original_size: 24},
    {tag: 'h3', max_size: 24, min_size: 20, original_size: 22},
    {tag: 'h4', max_size: 22, min_size: 18, original_size: 20},
    {tag: 'h5', max_size: 20, min_size: 16, original_size: 18},
    {tag: 'a', max_size: 18, min_size: 14, original_size: 16},
    {tag: 'p', max_size: 18, min_size: 14, original_size: 16},
    {tag: 'label', max_size: 18, min_size: 14, original_size: 16},
    {tag: 'span', max_size: 16, min_size: 12, original_size: 14},
    {tag: 'li', max_size: 16, min_size: 12, original_size: 14},
    {tag: 'input', max_size: 16, min_size: 12, original_size: 14}
];
function enlargeFont(){
    for (const element of tags) {
        tempTag = document.getElementsByTagName(element.tag);
        for (let i = 0; i < tempTag.length; i++) {
            style = window.getComputedStyle(tempTag[i], null).getPropertyValue('font-size');
            currentSize = parseFloat(style);
            if(currentSize <= element.max_size){
                tempTag[i].style.fontSize = (currentSize + 1) + 'px';
            }   
        }
    }
}

function resetFont(){
    for (const element of tags) {
        tempTag = document.getElementsByTagName(element.tag);
        for (let i = 0; i < tempTag.length; i++) {
            style = window.getComputedStyle(tempTag[i], null).getPropertyValue('font-size');
            currentSize = parseFloat(style);
            tempTag[i].style.fontSize = element.original_size + 'px';  
        }
    }
}

function diminishFont(){
    for (const element of tags) {
        tempTag = document.getElementsByTagName(element.tag);
        for (let i = 0; i < tempTag.length; i++) {
            style = window.getComputedStyle(tempTag[i], null).getPropertyValue('font-size');
            currentSize = parseFloat(style);
            if(currentSize >= element.min_size){
                tempTag[i].style.fontSize = (currentSize - 1) + 'px';
            }   
        }
    }
}
</script>
