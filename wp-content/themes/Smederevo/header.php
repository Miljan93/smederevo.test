<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link rel="preload" as="style" href="https://fonts.googleapis.com/css?family=Roboto:400">
<link rel="stylesheet" media="print" onload="this.onload=null;this.removeAttribute('media');" href="https://fonts.googleapis.com/css?family=Roboto:400">
<noscript>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400">
</noscript>
<?php wp_head(); ?>
<style>

html{
  margin: 0 !important;
  padding: 0 !important;
  font-size: 16px !important;
}

body {
  margin: 0 !important;
  padding: 0 !important;
  background: white;
  font-family: 'Roboto', sans-serif;
  line-height: 1.75;
  color: #000000;
}

h1, h2, h3, h4, h5 {
  margin: 2rem 0 1rem !important;
  font-family: 'Roboto', sans-serif;
  line-height: 1 !important;
}

h1 {
  font-size: 26px;
  font-weight: 600 !important;
  min-height: 0vw !important;
}

h2 {font-size: 24px;}
h3 {font-size: 22px;}
h4 {font-size: 20px;}
h5 {font-size: 18px;}
p, a{
  font-size: 16px;
  min-height: 0vw !important;
  /*margin-bottom: 0.5rem !important;*/
}
label{font-size: 16px;}
small, .text_small, span, li, {font-size: 14px;}

a:focus{
  -moz-box-shadow:    inset 0 0 2px #000000;
   -webkit-box-shadow: inset 0 0 2px #000000;
   box-shadow:         inset 0 0 2px #000000;
   /*box-shadow: inset -2px #000000;*/
}
#navigation{
  color: #fff !important;
}
#navigation a{
  text-decoration: none;
  margin: 0 !important;
  color: #fff !important;
}

#navigation ul{
  list-style: none;
  padding: 0;
  margin: 0;
}

main{
  color: #414D5D;
}

aside{
  color: #414D5D;
}

.accordion-button:focus{
  box-shadow: inset 0 0 0 0.25rem rgb(13 110 253 / 25%);
}
/* HEDER ZA MOBILNU VERZIJU */
header{
  top: 0;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  height: 80px;
  background-color: #2C394B;
}
.emblem-wrapper-mobile{
  position: absolute;
  display: flex;
  left: 20px;
  color: #fff;
}
.emblem-mobile{
    height: 60px;
}
.emblem-text-wrapper-mobile{
  margin-left: 10px;
}
.emblem-text-wrapper-mobile span{
  font-weight: bold;
}
.emblem-text-wrapper-mobile p{
  margin: 0 !important;
}
.emblem-text-wrapper-mobile hr{
  margin: 0 !important;
}

/* HEDER ZA MOBILNU VERZIJU - KRAJ */

/* NAVIGACIONI MENI */

#breadcrumbs-wrapper{
  display: flex;
  align-items: center;
  width: 100%;
  min-height: 60px;
  background-color: #414D5D;
  position: fixed;
  bottom: 60px;
  z-index: 100;
  color: #fff;
}
#breadcrumbs-wrapper a{
  color: #fff;
}
.emblem-container-mobile{
  display: contents;
}
.emblem-container-mobile a{
  display: contents;
}
#navigation{
  position: fixed;
  bottom: 0;
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  height: 60px;
  background-color: #2C394B;
  z-index: 100;
}
#navigation .container{
  min-height: 60px;
}
#menu-wrapper {
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: absolute;
  z-index: 1;
  -webkit-user-select: none;
  user-select: none;
  width: 60px;
  height: 60px;
  right: 0px;
}
#menu-wrapper > input {
  display: block;
  width: 60px;
  height: 60px;
  position: absolute;
  top: -7px;
  left: -5px;
  cursor: pointer;
  opacity: 0;
  z-index: 2; 
  -webkit-touch-callout: none;
}
#menu-wrapper span {
  display: block;
  width: 33px;
  height: 4px;
  margin-bottom: 5px;
  position: relative;
  background: #cdcdcd;
  border-radius: 3px;
  z-index: 1;
  transform-origin: 4px 0px;
  transition: transform 0.5s cubic-bezier(0.77, 0.2, 0.05, 1.0),
  background 0.5s cubic-bezier(0.77, 0.2, 0.05, 1.0),
  opacity 0.55s ease;
}
#menu-wrapper span:first-child {
  transform-origin: 0% 0%;
}
#menu-wrapper span:nth-last-child(2) {
  transform-origin: 0% 100%;
}
#menu-wrapper input:checked ~ span {
  opacity: 1;
  transform: rotate(45deg) translate(-2px, -1px);
}
#menu-wrapper input:checked ~ span:nth-last-child(3) {
  opacity: 0;
  transform: rotate(0deg) scale(0.2, 0.2);
}
#menu-wrapper input:checked ~ span:nth-last-child(2) {
  transform: rotate(-45deg) translate(0, -1px);
}
#menu-container {
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: absolute;
  width: 100vw;
  height: 100vh;
  top: 60px;
  right: 0px;
  margin: -100vh 0 0 -100vw;
  padding: 10vh 20px;
  background: #2C394B;
  list-style-type: none;
  -webkit-font-smoothing: antialiased;
  transform-origin: 0% 0%;
  transform: translate(105vw, 0);
  transition: transform 0.5s cubic-bezier(0.77, 0.2, 0.05, 1.0);
}
#menu-wrapper input:checked ~ label {
  transform: none;
}
.menu-item-container{
  border-radius: 5px;
  margin-bottom: 5px;
}
.menu-item-container a{
  text-decoration: none;
  color: #fff;
}
/*.menu-item-container:nth-of-type(1) label a{
    color: #b23850;
}
.menu-item-container:nth-of-type(2) label a{
    color: #3b8beb;
}
.menu-item-container:nth-of-type(3) label a{
    color: #EBDDB0;
}
.menu-item-container:nth-of-type(4) label a{
    color: #D3F574;
}
.menu-item-container:nth-of-type(5) label a{
    color: #8590AA;
}*/

.control-wrapper{
  display: flex;
  justify-content: center;
  align-items: center;
  width: 60px;
  height: 60px;
}
.subMenu{
  padding: 0 5px;
  display: block;
  line-height: 60px;
  cursor: pointer;
} 
.subMenu a{
  color: #2C394B;
  text-decoration: none;
  font-size: 16px;
  font-weight: bold;
}
.toggleBox{
  display:none;
}
.toggleBox:checked + .subMenu-content {
  opacity: 1;
  display: inherit;
  pointer-events: auto;
}
.subMenu-content {
  background-color: inherit;
  display: none;
  opacity: 0;
  transition: opacity 1s;
  margin-top: -5px;
  padding: 15px 0 15px 15px;
}
.subMenu-content a {
  text-decoration: none;
  color: #cdcdcd;
  font-size: 14px;
}
.subMenu-content li{
  padding: 0 15px !important;
  font-size: 14px !important;
  margin: 0 !important;
  line-height: 30px !important;
  list-style: none;
}
.subMenu-content li a{
  font-size: 14px !important;
}
#search-toggle-wrapper{
  position: absolute;
  display: flex;
  left: 60px;
  height: 60px;
  width: 60px;
  align-content: center;
  align-items: center;
  justify-content: center;
  cursor: pointer;
}
#search-toggle{
  display: none;
}
#search-toggle:checked #search{
  display: flex;
}
#search{
  display: none;
  align-content: center;
  justify-content: center;
  align-items: center;
  position: absolute;
  width: 100vw;
  height: 90vh;
  top: 0;
  left: 0;
  background: #fff;
}
.emblem-wrapper{
  position: relative;
  left: 10px;
  display: flex;
  color: #fff;
  z-index: 10;
}
.emblem{
  height: 50px;
}
.emblem-text-wrapper{
  display:none;
  margin-left: 10px;
}
.emblem-text-wrapper p{
  margin: 0 !important;
}
.emblem-text-wrapper span{
  font-weight: bold;
  margin: 0 !important;
}
.emblem-text-wrapper hr{
  margin: 0 !important;
}
#font-size-container{
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: absolute;
  height: 60px;
  width: 60px;
  left: 170px;
}
#font-size-container a{
  display: flex;
  justify-content: center;
  align-items: center; 
  width: 60px;
  height: 60px;
}
#font-size{
  width: 30px;
} 
#font-size-controls{
  display: none;
  flex-direction: column;
  position: absolute;
  width: 60px;
  background-color: #2C394B;
  bottom: 60px;
}
#font-size-controls img{
    width: 20px;
}
#language-select-container{
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 60px;
  height: 60px;
  left: 90px;
}
#language-select-container>a{
  display: flex;
  justify-content: center;
  align-items: center;
  width: 60px;
  height: 60px;
}
#language-select-image{
    width: 30px;
}
#language-select-checkbox{
    display: block;
    position: absolute;
    opacity: 0;
    width: 60px;
    height: 60px;
}
#language-select-container input:checked ~ #language-select-controls{
    display: flex;
}
#language-select-controls{
    display: none;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    position: absolute;
    padding: 0 5px;
    min-width: 100px;
    bottom: 60px;
    background-color: #2C394B;
}
#language-select-controls li{
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 60px;
}
#language-select-controls li a{
    text-decoration: none;
    color: #fff;
}
.margin-top{
  margin-top: 80px;
}
@media only screen and (min-width: 1024px) {
  header{
    display: none;
  }
  #navigation{
      top: 0;
      height: 80px;
  }
  #breadcrumbs-wrapper{
    min-height: unset;
    height: 30px;
    bottom: unset;
    top: 80px;
  }
  #menu-wrapper{
      position: relative;
      width: 100%;
      justify-content: flex-start;
      right: 20px;
  }
  #menu-wrapper input{
      display: none;
  }
  #menu-wrapper span{
      display: none;
  }
  #menu-container {
      position: unset;
      width: 100%;
      height: 80px;
      top: 0px;
      margin: 0px;
      padding: 0px;
      transform: none;
      display: flex;
      flex-direction: row;
      justify-content: flex-end;
  }
  .menu-item-container{
      display: inline-block;
      border-radius: 0px;
      margin-bottom: 0px;
  }
  .toggleBox:checked + .subMenu-content {
      opacity: 0;
      display: none;
      pointer-events: none;
  }
  .menu-item-container:hover > .subMenu-content{
      position: absolute;
      left: 0;
      display: block;
      width: 100vw;
      opacity: 1;
      transition: opacity 1s;
      margin-top: -5px;
      padding: 15px 0 15px 15px;
  }
  .emblem-wrapper{
      position: absolute;
      left: 20px;
      display: flex;
      color: #fff;
  }
  .emblem-text-wrapper{
    display:flex;
    flex-direction: column;
    justify-content: center;
    margin-left: 10px;
  }
  .emblem{
      height: 60px;
  }
  #font-size-container{
    position: relative;
    left: unset;
    right: 10px;
  }
  #font-size-controls{
      bottom: unset;
      top: 60px;
  }
  #language-select-container{
      position: relative;
      left: unset;
      right: 30px;
  }
  #language-select-controls{
      bottom: unset;
      top: 60px;
  }
  .margin-top{
    margin-top: 160px;
  }
}

/* NAVIGACIONI MENI - KRAJ */
</style>
</head>
<body <?php body_class();?>>
<?php get_template_part('partials/nav-menu'); ?>
