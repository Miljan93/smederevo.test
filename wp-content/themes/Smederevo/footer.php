    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h5><?php echo pll_e('Контакт') ?></h5>
                <ul>
                    <li><?php echo pll_e('Омладинска 1') ?></li>
                    <li><?php echo pll_e('11300 Смедерево') ?></li>
                    <li>026/672-724</li>
                    <li></li>
                </ul>
                </div>
                <div class="col-md-6">

                </div>
            </div>
        </div>
    </footer>
    <?php 
    require_once dirname( __FILE__ ) . '/js-functions/toggle.php';
    require_once dirname( __FILE__ ) . '/js-functions/font-manipulation.php';
    wp_footer(); ?>
</body>
</html>