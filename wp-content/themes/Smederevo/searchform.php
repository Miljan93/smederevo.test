<style>
    .search-form{
        display: inline-flex;
        width: 100%;
    }
    .search-submit-wrapper{
        display: inline-flex;
        background-color: #2C394B;
        width: 40px;
        height: 40px;
        text-align: center;
        justify-content: center;
        align-items: center;
        cursor: pointer;
    }
    .search-input-placeholder{
        width: 100%;
    }
    .search-input-placeholder input{
        position: relative;
        background-color: #fff;
        width: 100%;
        height: 40px;
        color: #000;
        border: 1px solid #000;
    }
    .search-input-placeholder input:focus-visible{
        border: none;
    }
    #search-submit{
        display: none;   
    }
</style>
<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <div class="search-input-placeholder">
        <input style="display: inline-block;" type="search" class="search-field"
            placeholder="<?php echo pll_e('Упишите термин претраге...')?>"
            value="<?php echo get_search_query() ?>" name="s"
            title="<?php echo esc_attr_x( 'Претражи:', 'label' ) ?>" />
    </div>
    <label class="search-submit-wrapper" for="search-submit">
        <input id="search-submit" type="submit" class="search-submit"
        value="<?php echo esc_attr_x( 'Pretraži', 'submit button' ) ?>" />
        <img width="24px" height="24px" src="<?php echo get_template_directory_uri(); ?>/assets/icons/Search.svg">
    </label>
</form>